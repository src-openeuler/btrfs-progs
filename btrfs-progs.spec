Name:       btrfs-progs
Version:    6.6.3
Release:    15
Summary:    btrfs userspace programs
License:    GPLv2 and GPL+ and LGPL-2.1+ and GPL-3.0+ and LGPL-2.1 and MIT
URL:        https://btrfs.wiki.kernel.org/index.php/Main_Page
Source0:    https://www.kernel.org/pub/linux/kernel/people/kdave/%{name}/%{name}-v%{version}.tar.xz

Patch0001:  0001-fix-exclusive-op-enqueue-timeout.patch
Patch0002:  0002-subvolume-fix-return-value-when-the-target-exists.patch
Patch0003:  0003-fix-memory-leak-on-exit-path-in-table-vprintf.patch
Patch0004:  0004-btrfs-progs-scrub-status-only-report-limits-if-at-le.patch
Patch0005:  0005-btrfs-progs-fix-freeing-of-device-after-error-in-btr.patch
Patch0006:  0006-fix-double-free-on-error-in-read_raid56.patch
Patch0007:  0007-btrfs-progs-fi-show-canonicalize-path-when-using-blk.patch
Patch0008:  0008-btrfs-progs-tune-fix-the-missing-close-of-filesystem.patch
Patch0009:  0009-btrfs-progs-error-out-immediately-if-an-unknown-back.patch
Patch0010:  0010-btrfs-progs-fix-the-conflicting-super-block-flags.patch
Patch0011:  0011-btrfs-progs-convert-for-ext2-fix-possible-tree-check.patch
Patch0012:  0012-btrfs-progs-convert-insert-a-dummy-inode-item-before.patch
Patch0013:  0013-btrfs-progs-corrupt-block-fix-memory-leak-in-debug_c.patch
Patch0014:  0014-btrfs-progs-image-fix-the-bug-that-filename-sanitiza.patch
Patch0015:  0015-btrfs-progs-convert-fix-the-filename-output-when-rol.patch

BuildRequires:  python3-devel >= 3.4
BuildRequires:  libacl-devel, e2fsprogs-devel, libblkid-devel, libuuid-devel, zlib-devel, libzstd-devel, lzo-devel, systemd-devel
BuildRequires:  gcc, asciidoc, systemd, xmlto, autoconf, automake, python3-sphinx

%define _root_sbindir /sbin

%description
Btrfs-progs provide userspace utilities to manage btrfs filesystem, including
repairing, modifying, creating etc.

%package        devel
Summary:        Including header files and libraries for developing btrfs related programs
Requires:       %{name} = %{version}-%{release}

%description    devel
This package includes header files and libraries for developing btrfs related programs

%package        help
Summary:        Including man files for btrfs-progs
BuildArch:      noarch
Requires:       man

%description    help
This package includes man files for btrfs-progs

%prep
%autosetup -n %{name}-v%{version} -p1

%build
./autogen.sh
%configure CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
%make_build

%install
make mandir=%{_mandir} bindir=%{_sbindir} libdir=%{_libdir} incdir=%{_includedir} install DESTDIR=%{buildroot}

%files
%license COPYING
%{_libdir}/libbtrfs.so.0*
%{_sbindir}/btrfsck
%{_sbindir}/fsck.btrfs
%{_sbindir}/mkfs.btrfs
%{_sbindir}/btrfs-image
%{_sbindir}/btrfs-convert
%{_sbindir}/btrfs-select-super
%{_sbindir}/btrfstune
%{_sbindir}/btrfs
%{_sbindir}/btrfs-map-logical
%{_sbindir}/btrfs-find-root
%{_udevrulesdir}/64-btrfs-dm.rules
%{_udevrulesdir}/64-btrfs-zoned.rules
%exclude %{_libdir}/*.a

%files devel
%{_includedir}/*
%{_libdir}/libbtrfs.so
%{_libdir}/libbtrfsutil.so*
%{_libdir}/pkgconfig/libbtrfsutil.pc

%files help
%{_mandir}/man5/*.gz
%{_mandir}/man8/*.gz

%changelog
* Mon Oct 28 2024 liuh <liuhuan01@kylinos.cn> - 6.6.3-15
- btrfs-progs: convert: fix the filename output when rolling back

* Tue Sep 10 2024 liuh <liuhuan01@kylinos.cn> - 6.6.3-14
- btrfs-progs: image: fix the bug that filename sanitization not working

* Fri Sep 6 2024 cenhuilin <cenhuilin@kylinos.cn> - 6.6.3-13
- btrfs-progs: corrupt-block: fix memory leak in debug_corrupt_sector()

* Tue Sep 3 2024 liuh <liuhuan01@kylinos.cn> - 6.6.3-12
- btrfs-progs: convert: insert a dummy inode item before inode ref for ext2/4
- btrfs-progs: convert: for ext2, fix possible tree-checker error when converting a large fs

* Tue Aug 27 2024 liuh <liuhuan01@kylinos.cn> - 6.6.3-11
- btrfs-progs: fix the conflicting super block flags

* Wed Jul 17 2024 liuh <liuhuan01@kylinos.cn> - 6.6.3-10
- btrfs-progs: error out immediately if an unknown backref type is found

* Tue Jul 2 2024 cenhuilin <cenhuilin@kylinos.cn> - 6.6.3-9
- btrfs-progs: tune: fix the missing close() of filesystem fd

* Mon Jul 1 2024 liuh <liuhuan01@kylinos.cn> - 6.6.3-8
- btrfs-progs: fi show: canonicalize path when using blkid and -d

* Mon Jun 24 2024 zhangyaqi <zhangyaqi@kylinos.cn> - 6.6.3-7
- fix double free on error in read_raid56()

* Fri Jun 21 2024 liuh <liuhuan01@kylinos.cn> - 6.6.3-6
- backport patch fix null dereference risk

* Thu Jun 6 2024 liuh <liuhuan01@kylinos.cn> - 6.6.3-5
- backport patch from community

* Wed May 22 2024 Deyuan Fan <fandeyuan@kylinos.cn> - 6.6.3-4
- btrfs-progs: string-table: fix memory leak on exit path in table_vprintf()

* Wed May 8 2024 Deyuan Fan <fandeyuan@kylinos.cn> - 6.6.3-3
- btrfs-progs: subvolume: fix return value when the target exists

* Fri Apr 19 2024 cenhuilin <cenhuilin@kylinos.cn> - 6.6.3-2
- fix exclusive op enqueue timeout

* Wed Jan 17 2024 wuguanghao <wuguanghao3@huawei.com> - 6.6.3-1
- upgrade version to 6.6.3

* Thu Oct 26 2023 li weigang - <weigangli99@gmail.com> - 6.5.3-1
- update to version v6.5.3
- modify yaml

* Thu Feb 9 2023 lihaoxiang <lihaoxiang9@huawei.com> - 6.1.2-1
- upgrade to 6.1.2

* Mon Oct 24 2022 zhanchengbin <zhanchengbin1@huawei.com> - 6.0-1
- Update to btrfs-progs-6.0

* Tue Oct 18 2022 zhanchengbin <zhanchengbin1@huawei.com> - 5.15-2
- delete license Artistic-1.0-Perl

* Tue Nov 16 2021 Wenchao Hao <haowenchao@huawei.com> - 5.15-1
- Update to btrfs-progs-5.15

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 5.7-4
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Thu Dec 10 2020 yanglongkang<yanglongkang@huawei.com> - 5.7-3
- fix URL error

* Thu Nov 26 2020 lixiaokeng<lixiaokeng@huawei.com> - 5.7-2
- backport patches from epoch2

* Wed Jul 15 2020 Zhiqiang Liu <lzhq28@mail.ustc.edu.cn> - 5.7-1
- update to v5.7 version

* Wed Aug 28 2019 luoshijie <luoshijie1@huawei.com> - 4.17.1-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:openEuler Debranding

* Mon Aug 12 2019 luoshijie <luoshijie1@huawei.com> - 4.17.1-2
- Package init

